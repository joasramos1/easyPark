package edu.cest.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import edu.cest.dao.mapper.FuncionarioRowMapper;
import edu.cest.model.Funcionario;

@Repository
public class FuncionarioDAOImpl implements FuncionarioDAO {

	@Autowired
	private JdbcTemplate jdbc;

	@Override
	public List<Funcionario> findAll() {
		return jdbc.query("SELECT * FROM funcionario", new FuncionarioRowMapper());
	}

	@Override
	public Funcionario findByLogin(String user, String pass) {
		try {
			return this.jdbc.queryForObject("SELECT * FROM funcionario WHERE login = ? AND senha = ?",
					new FuncionarioRowMapper(), user, pass);
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	@Override
	public int save(Funcionario f) {
		if (f.getId() <= 0) {
			// insert
			return this.jdbc.update("INSERT INTO funcionario (login, nome, senha, telefone) VALUES(?,?,?,?)",
					f.getLogin(), f.getNome(), f.getSenha(), f.getTelefone());
		} else {
			// update
			return this.jdbc.update(
					"UPDATE funcionario SET login=?, nome=?, senha=?, telefone=?, isAdministrador=? "
							+ "WHERE idfuncionario=?",
					f.getLogin(), f.getNome(), f.getSenha(), f.getTelefone(), f.isAdministrador(), f.getId());
		}
	}

	@Override
	public int delete(Integer id) {
		if (id > 0) {
			return this.jdbc.update("DELETE FROM funcionario WHERE idfuncionario = ?", id);
		}
		return 0;
	}

	@Override
	public Funcionario findById(Integer id) {
		return this.jdbc.queryForObject("SELECT * FROM funcionario WHERE idfuncionario = ?", new FuncionarioRowMapper(), id);
	}

}
