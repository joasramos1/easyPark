package edu.cest.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import edu.cest.dao.mapper.VeiculoRowMapper;
import edu.cest.model.Veiculo;

@Repository
public class VeiculoDAOImpl implements VeiculoDAO {

	@Autowired
	private JdbcTemplate jdbc;

	@Override
	public List<Veiculo> findAll() {
		return jdbc.query("SELECT * FROM veiculo", new VeiculoRowMapper());
	}

	@Override
	public Veiculo findById(int id, String placa) {
		try {
			return this.jdbc.queryForObject("SELECT * FROM veiculo WHERE idveiculo = ? AND placa = ?",
					new VeiculoRowMapper(), id, placa);
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	@Override
	public int save(Veiculo ve) {
		if (ve.getId() <= 0) {
			// insert
			return this.jdbc.update("INSERT INTO veiculo (cor, placa) VALUES(?,?)", ve.getCor(), ve.getPlaca());
		} else {
			// update
			return this.jdbc.update("UPDATE funcionario SET cor=?, placa=? " + "WHERE idveiculo=?", ve.getCor(),
					ve.getPlaca(), ve.getId());
		}
	}

	@Override
	public int delete(Integer id) {
		if (id > 0) {
			return this.jdbc.update("DELETE FROM veiculo WHERE idveiculo = ?", id);
		}
		return 0;
	}

	@Override
	public Veiculo findById(Integer id) {
		return this.jdbc.queryForObject("SELECT * FROM veiculo WHERE idveiculo = ?", new VeiculoRowMapper(), id);
	}

	@Override
	public int getMaxId() {
		return this.jdbc.queryForObject("SELECT max(idveiculo) FROM veiculo", int.class);
	}

	@Override
	public Veiculo findByPlaca(String placa) {

		Veiculo v = null;

		try {
			v = this.jdbc.queryForObject("SELECT * FROM veiculo WHERE placa = ?", new VeiculoRowMapper(), placa);
			return v;
		} catch (Exception e) {
			return v;
		}
		
	}

}
