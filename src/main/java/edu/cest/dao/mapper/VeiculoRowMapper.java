package edu.cest.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import edu.cest.model.Veiculo;

public class VeiculoRowMapper implements RowMapper<Veiculo> {

	@Override
	public Veiculo mapRow(ResultSet rs, int rowNum) throws SQLException {
		Veiculo ve = new Veiculo();
		
		ve.setId(rs.getInt("idveiculo"));
		ve.setCor(rs.getString("cor"));
		ve.setPlaca(rs.getString("placa")); 
		
		return ve;
	}

}
