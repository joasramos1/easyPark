package edu.cest.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import edu.cest.model.Faturamento;


public class FaturamentoRowMapper implements RowMapper<Faturamento>{

	@Override
	public Faturamento mapRow(ResultSet rs, int rowNum) throws SQLException {
		Faturamento ft = new Faturamento();
		
		ft.setIdfuncionario(rs.getInt("idfuncionario"));
		ft.setIdestadia(rs.getInt("idestadia"));
		ft.setId(rs.getInt("idfaturamento"));
		ft.setValor(rs.getDouble("valor")); 
		ft.setObservacao(rs.getString("observacao")); 
		ft.setDataHoraPagamento(rs.getDate("dataHoraPagamento"));	
		
		return ft;
	}

}
