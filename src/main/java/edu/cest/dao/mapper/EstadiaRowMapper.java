package edu.cest.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import edu.cest.model.Estadia;



public class EstadiaRowMapper implements RowMapper <Estadia> {
	
	@Override
	public Estadia mapRow(ResultSet rs, int rowNum) throws SQLException {
		Estadia e = new Estadia();
		
		e.setId(rs.getInt("idestadia"));
		e.setIdfuncionario(rs.getInt("idfuncionario")); 
		e.setIdvaga(rs.getInt("idvaga")); 
		e.setIdveiculo(rs.getInt("idveiculo")); 
		e.setDataHoraEntrada(rs.getDate("dataHoraEntrada"));	
		e.setDataHoraSaida(rs.getDate("dataHoraSaida"));	
		
		return e;
	}

}
