package edu.cest.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import edu.cest.model.Vaga;

public class VagaRowMapper implements RowMapper<Vaga> {

	@Override
	public Vaga mapRow(ResultSet rs, int rowNum) throws SQLException {
		Vaga v = new Vaga();

		v.setId(rs.getInt("idvaga"));
		v.setNumero(rs.getInt("numero"));
		v.setSetor(rs.getString("setor")); 
		v.setAtivo(rs.getBoolean("ativo")); 
 
		return v;
	}

}
