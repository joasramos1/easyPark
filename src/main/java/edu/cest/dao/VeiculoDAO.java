package edu.cest.dao;

import java.util.List;


import edu.cest.model.Veiculo;

public interface VeiculoDAO {
	
	/**
	 * 
	 * @return Retorna uma lista de Vagas
	 */
	List<Veiculo> findAll();

	
	/**
	 * 
	 * @param f - Veiculo a ser persistido
	 * @return
	 */
	int save(Veiculo ve);
	
	/**
	 * 
	 * @param id - id da Veiculo que ser� excluida
	 * @return
	 */
	int delete(Integer id);
	
	
	/**
	 * Busca Veiculo por ID
	 * @param id - Id da Veiculo
	 * @return Veiculo
	 */
	Veiculo findById(Integer id);

	/**
	 * 
	 * @param id
	 * @param placa
	 * @return
	 */
	Veiculo findById(int id, String placa);
	
	/**
	 * 
	 * @return
	 */
	int getMaxId();
	
	
	/**
	 * 
	 * @param placa
	 * @return
	 */
	Veiculo findByPlaca(String placa);


}
