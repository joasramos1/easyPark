package edu.cest.dao;

import java.util.List;

import edu.cest.model.Vaga;

public interface VagaDAO {
	
	/**
	 * 
	 * @return Retorna uma lista de Vagas
	 */
	List<Vaga> findAll();

	/**
	 * 
	
	 * @return - Objeto Vaga
	 */
	VagaDAO findBySetor(String numero, String setor);
	
	/**
	 * 
	 * @param f - Vaga a ser persistido
	 * @return
	 */
	int save(Vaga v);
	
	/**
	 * 
	 * @param id - id da vaga que ser� excluida
	 * @return
	 */
	int delete(Integer id);
	
	
	/**
	 * Busca Vaga por ID
	 * @param id - Id da Vaga
	 * @return Vaga
	 */
	Vaga findById(Integer id);

	/**
	 * 
	 * @param numero
	 * @param setor
	 * @return
	 */
	Vaga findBySetor(String numero, char setor);
	

	/**
	 * 
	 * @return
	 */
	List<Vaga> findBusy();

	/**
	 * 
	 * @return
	 */
	List<Vaga> findFree();
	
	
	/**
	 * 
	 * @param idvaga
	 */
	void updateVagaBusy(int idvaga);
	
	/**
	 * 
	 * @param idvaga
	 */
	void updateVagaFree(int idvaga);

}
