package edu.cest.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import edu.cest.dao.mapper.EstadiaRowMapper;
import edu.cest.model.Estadia;

@Repository
public class EstadiaDAOImpl implements EstadiaDAO {

	@Autowired
	private JdbcTemplate jdbc;

	@Override
	public List<Estadia> findAll() {
		return jdbc.query("SELECT * FROM estadia", new EstadiaRowMapper());

	}

	@Override
	public Estadia findById(String id) {
		try {
			return this.jdbc.queryForObject("SELECT * FROM estadia WHERE idestadia = ?", new EstadiaRowMapper(), id);
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	@Override
	public int save(Estadia e) {
		if (e.getId() <= 0) {
			// insert
			return this.jdbc.update("INSERT INTO estadia (idfuncionario, idvaga, idveiculo) VALUES(?,?,?)",
					e.getIdfuncionario(), e.getIdvaga(), e.getIdveiculo());
		} else {
			// update
			return this.jdbc.update(
					"UPDATE estadia SET idfuncionario=?, idvaga=?, dataHoraEntrada=?, dataHoraSaida=?"
							+ "WHERE idestadia=?",
					e.getIdfuncionario(), e.getIdvaga(), e.getDataHoraEntrada(), e.getDataHoraSaida(), e.getId());
		}
	}

	@Override
	public int delete(Integer id) {
		if (id > 0) {
			return this.jdbc.update("DELETE FROM estadia WHERE idestadia = ?", id);
		}
		return 0;
	}

	@Override
	public Estadia findById(Integer id) {
		Estadia e = null;
		try {
			e = this.jdbc.queryForObject("SELECT * FROM estadia WHERE idestadia = ?", new EstadiaRowMapper(), id);
		} catch (Exception ex) {
			System.out.println("IDESTADIA:" + id);
		}

		return e;
	}

	@Override
	public Estadia findByVeiculoWhereVagaTrue(int idveiculo) {

		Estadia e = null;

		try {
			e = this.jdbc.queryForObject(
					"SELECT e.* FROM estadia e " + "JOIN veiculo v ON v.idveiculo = e.idveiculo "
							+ "JOIN vaga va ON va.idvaga = e.idvaga " + "WHERE va.ativo = 0 AND v.idveiculo = ?",
					new EstadiaRowMapper(), idveiculo);
			return e;
		} catch (Exception ex) {
			return e;
		}

	}

}
