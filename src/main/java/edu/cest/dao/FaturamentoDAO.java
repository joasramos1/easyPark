package edu.cest.dao;

import java.util.List;

import edu.cest.model.Faturamento;

public interface FaturamentoDAO {
	
	/**
	 * 
	 * @return Retorna uma lista de faturamento
	 */
	List<Faturamento> findAll();

	/**
	 * 

	 * @return - Objeto Faturamento
	 */
	Faturamento findById(int id, double valor);
	
	/**
	 * 
	 * @param ft - faturamento a ser persistido
	 * @return
	 */
	int save(Faturamento ft);
	
	/**
	 * 
	 * @param id - id do faturamento que ser� excluido
	 * @return
	 */
	int delete(Integer id);
	
	
	/**
	 * Busca faturamneto por ID
	 * @param id - Id do Faturamento
	 * @return Faturamento
	 */
	Faturamento findById(Integer id);
	

}
