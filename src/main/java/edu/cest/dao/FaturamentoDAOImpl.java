package edu.cest.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import edu.cest.dao.mapper.FaturamentoRowMapper;
import edu.cest.model.Faturamento;

@Repository
public class FaturamentoDAOImpl implements FaturamentoDAO{
	
	@Autowired
	private JdbcTemplate jdbc;

	@Override
	public List<Faturamento> findAll() {
		return jdbc.query("SELECT * FROM faturamento", new FaturamentoRowMapper());
	}

	@Override
	public Faturamento findById(int id, double valor) {
		try {
			return this.jdbc.queryForObject("SELECT * FROM faturamento WHERE id = ? AND valor = ?",
					new FaturamentoRowMapper(), id, valor);
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	@Override
	public int save(Faturamento ft) {
		if (ft.getId() <= 0) {
			// insert
			return this.jdbc.update("INSERT INTO faturamento (idestadia, idfuncionario, valor, observacao, dataHoraPagamento) VALUES(?,?,?,?,?)",
					ft.getIdestadia(), ft.getIdfuncionario(),ft.getValor(),ft.getObservacao(),ft.getDataHoraPagamento());
		} else {
			// update
			return this.jdbc.update(
					"UPDATE faturamento SET idestadia=?, idfuncionario=?, valor=?, observacao=?, dataHoraPagamento=? "
							+ "WHERE idfaturamento=?",
					ft.getIdestadia(),ft.getIdfuncionario(), ft.getValor(), 
					ft.getObservacao(), ft.getDataHoraPagamento(),ft.getId());
		}
	}

	@Override
	public int delete(Integer id) {
		if (id > 0) {
			return this.jdbc.update("DELETE FROM faturamento WHERE idfaturamento = ?", id);
		}
		return 0;
	}

	@Override
	public Faturamento findById(Integer id) {
		return this.jdbc.queryForObject("SELECT * FROM faturamento WHERE id = ?", new FaturamentoRowMapper(), id);
	}

}
