package edu.cest.dao;

import java.util.List;

import edu.cest.model.Estadia;


public interface EstadiaDAO {
	

	/**
	 * 
	 * @return Retorna uma lista de estadias
	 */
	List<Estadia> findAll();

	/**
	 * 
	
	 * @return - Objeto Estadia
	 */
	Estadia findById(String id);
	
	/**
	 * 
	 * @param f - Estadia a ser persistido
	 * @return
	 */
	int save(Estadia e);
	
	/**
	 * 
	 * @param id - id do estadia que ser� excluido
	 * @return
	 */
	int delete(Integer id);
	
	
	/**
	 * Busca estadia por ID
	 * @param id - Id do estadia
	 * @return estadia
	 */
	Estadia findById(Integer id);
	
	
	/**
	 * 
	 * @param idveiculo
	 * @return
	 */
	Estadia findByVeiculoWhereVagaTrue(int idveiculo);

}
