package edu.cest.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import edu.cest.dao.mapper.VagaRowMapper;
import edu.cest.model.Vaga;

@Repository
public class VagaDAOImpl implements VagaDAO {

	@Autowired
	private JdbcTemplate jdbc;

	@Override
	public List<Vaga> findAll() {
		return jdbc.query("SELECT * FROM vaga", new VagaRowMapper());
	}

	@Override
	public Vaga findBySetor(String numero, char setor) {
		try {
			return this.jdbc.queryForObject("SELECT * FROM vaga WHERE numero = ? AND setor = ?", new VagaRowMapper(),
					numero, setor);
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	@Override
	public int save(Vaga v) {
		if (v.getId() <= 0) {
			return this.jdbc.update("INSERT INTO vaga (numero, setor) VALUES( ? , ? )", v.getNumero(), v.getSetor());
			//INSERT INTO `easypark`.`vaga` (`numero`, `setor`) VALUES ('1', 'B');

		} else {
			return this.jdbc.update("UPDATE vaga SET numero=?, setor=? " + "WHERE idvaga=?", v.getNumero(),
					v.getSetor());
		}
	}

	@Override
	public int delete(Integer id) {
		if (id > 0) {
			return this.jdbc.update("DELETE FROM vaga WHERE idvaga = ?", id);
		}
		return 0;
	}

	@Override
	public Vaga findById(Integer id) {
		return this.jdbc.queryForObject("SELECT * FROM vaga WHERE idvaga = ?", new VagaRowMapper(), id);
	}

	@Override
	public VagaDAO findBySetor(String numero, String setor) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public List<Vaga> findBusy() {
		return jdbc.query("SELECT * FROM vaga WHERE ativo = 0", new VagaRowMapper());
	}
	
	public List<Vaga> findFree() {
		return jdbc.query("SELECT * FROM vaga WHERE ativo = 1", new VagaRowMapper());
	}

	@Override
	public void updateVagaBusy(int id) {
		
		this.jdbc.update("UPDATE vaga SET ativo = 0 WHERE idvaga = ? ", id);
		
	}
	
	@Override
	public void updateVagaFree(int id) {
		
		this.jdbc.update("UPDATE vaga SET ativo = 1 WHERE idvaga = ? ", id);
		
	}

	
}
