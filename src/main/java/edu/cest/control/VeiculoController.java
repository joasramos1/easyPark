package edu.cest.control;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import edu.cest.dao.VeiculoDAO;
import edu.cest.model.Veiculo;

@Controller
public class VeiculoController {

	@Autowired
	private VeiculoDAO veDAO;

	@RequestMapping("/veiculo")
	public String index(Model m) {

		List<Veiculo> l = veDAO.findAll();

		m.addAttribute("veiculo", l);

		return "veiculo/lista";
	}

	/**
	 * 
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/veiculo/{id}", method = RequestMethod.GET)
	public String editaVeiculo(@PathVariable(value = "id") Integer id, Model m) {
		Veiculo ve = veDAO.findById(id);

		// preenchendo valores da vaga que ser�o exibidos na view html
		m.addAttribute("pageTitle", "Editar Estado");
		m.addAttribute("id", ve.getId());
		m.addAttribute("placa", ve.getPlaca());
		m.addAttribute("cor", ve.getCor());
	

		return "veiculo/edita";
	}

	@RequestMapping(value = "/veiculo/{id}", method = RequestMethod.POST)
	public String atualizaVeiculo(@PathVariable(value = "id") Integer id, @RequestParam String placa,
			@RequestParam String cor,Model m) {

		Veiculo ve = veDAO.findById(id);
		ve.setPlaca(placa);
		ve.setCor(cor);
		

		veDAO.save(ve);

		return "redirect:/veiculo";
	}

	@RequestMapping(value = "/veiculo/delete/{id}", method = RequestMethod.GET)
	public String deletaVeiculo(@PathVariable(value = "id") Integer id, Model m) {

		veDAO.delete(id);

		return "redirect:/veiculo";
	}

	/**
	 * N�o precisa de RequestMapping
	 * 
	 * @param v
	 *            - veiculo
	 * @param m
	 *            - Model
	 * @return
	 */
	public Model getModel(Veiculo ve, Model m) {
		m.addAttribute("id", ve.getId());
		return m;
	}
}
