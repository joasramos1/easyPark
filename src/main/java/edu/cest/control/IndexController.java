package edu.cest.control;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import config.Usuario;
import edu.cest.dao.FuncionarioDAO;
import edu.cest.dao.VagaDAO;
import edu.cest.model.Funcionario;
import edu.cest.model.Vaga;

@Controller
public class IndexController{

	@Autowired
	private FuncionarioDAO funcionarioDAO;

	@Autowired
	private VagaDAO vDAO;

	private FuncionarioController funcionarioController;

	@RequestMapping("/")
	public String index(Model m) {
		m.addAttribute("authenticUrl", "/validaLogin");
		return "home/index";
	}

	@RequestMapping(value = "/validaLogin", method = RequestMethod.POST)
	public String validaLogin(@RequestParam String username, @RequestParam String password, Model m) {

		Funcionario f = funcionarioDAO.findByLogin(username, password);
		Usuario.setFuncionario(f); 

		if (f != null) {

			funcionarioController = new FuncionarioController();

			m = funcionarioController.getModel(f, m);

			if (f.isAdministrador()) {			
				
				List<Vaga> free = vDAO.findFree();
				List<Vaga> vagas = vDAO.findAll();
				m.addAttribute("vagasFree", free);
				m.addAttribute("vagas", vagas);
								
				return "dashboard/admin";

			} else {

				return "dashboard/funcionario";

			}
		}

		m.addAttribute("errorLogin", "true");

		return "home/index";
	}

	/**
	 * M�todo temporario - dever� ser excluido brevemente
	 * 
	 * @return
	 */
	@RequestMapping("/admin")
	public String homeTemp(Model m) {

		List<Vaga> free = vDAO.findFree();
		List<Vaga> vagas = vDAO.findAll();
		m.addAttribute("vagasFree", free);
		m.addAttribute("vagas", vagas);

		return "dashboard/admin";
	}
}
