package edu.cest.control;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import edu.cest.dao.VagaDAO;
import edu.cest.model.Vaga;

@Controller
public class VagaController {

	@Autowired
	private VagaDAO vDAO;

	@RequestMapping("/vaga")
	public String index(Model m) {

		List<Vaga> l = vDAO.findAll();

		m.addAttribute("vagas", l);

		return "vaga/lista";
	}

	/**
	 * 
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/vaga/{id}", method = RequestMethod.GET)
	public String editaVaga(@PathVariable(value = "id") Integer id, Model m) {
		Vaga v = vDAO.findById(id);

		m.addAttribute("pageTitle", "Editar Estado");
		m.addAttribute("id", v.getId());
		m.addAttribute("setor", v.getSetor());
		m.addAttribute("numero", v.getNumero());
		m.addAttribute("ativo", v.isAtivo());

		return "vaga/edita";
	}

	@RequestMapping(value = "/vaga/salvar/", method = RequestMethod.POST)
	public String salvar(@RequestParam String setor, @RequestParam int numero) {

		Vaga v = new Vaga();
		
		v.setSetor(setor); 
		
		v.setNumero(numero);

		vDAO.save(v);

		return "redirect:/vaga";
	}

	@RequestMapping(value = "/vaga/{id}", method = RequestMethod.POST)
	public String atualizaVaga(@PathVariable(value = "id") Integer id, @RequestParam String setor,
			@RequestParam int numero, @RequestParam boolean ativo, Model m) {

		Vaga v = vDAO.findById(id);
		
		v.setSetor(setor);
		
		v.setNumero(numero);

		vDAO.save(v);

		return "redirect:/vaga";
	}

	@RequestMapping(value = "/vaga/delete/{id}", method = RequestMethod.GET)
	public String deletaVaga(@PathVariable(value = "id") Integer id, Model m) {

		vDAO.delete(id);

		return "redirect:/vaga";
	}

	/**
	 * N�o precisa de RequestMapping
	 * 
	 * @param v
	 *            - Vaga
	 * @param m
	 *            - Model
	 * @return
	 */
	public Model getModel(Vaga v, Model m) {
		m.addAttribute("id", v.getId());
		return m;
	}
	
	/**
	 * 
	 * @return
	 */
	@RequestMapping("vaga/showForm")
	public String showForm() {
		return "vaga/novo";
	}

}
