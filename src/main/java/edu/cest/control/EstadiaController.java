package edu.cest.control;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.j2objc.annotations.AutoreleasePool;

import config.Usuario;
import edu.cest.dao.EstadiaDAO;
import edu.cest.dao.FaturamentoDAO;
import edu.cest.dao.VagaDAO;
import edu.cest.dao.VeiculoDAO;
import edu.cest.model.Estadia;
import edu.cest.model.Faturamento;
import edu.cest.model.Veiculo;

@Controller
public class EstadiaController {
	@Autowired
	private EstadiaDAO eDAO;

	@Autowired
	private VeiculoDAO vDAO;

	@Autowired
	private VagaDAO vagaDAO;

	@Autowired
	private FaturamentoDAO fDAO;

	@RequestMapping("/estadia")
	public String index(Model m) {

		List<Estadia> l = eDAO.findAll();

		m.addAttribute("estadias", l);

		return "estadia/lista";
	}

	@PostMapping(value = "/registrarCheckIn")
	public String registrarEntrada(@RequestParam String cor, @RequestParam String placa, @RequestParam int idvaga,
			Model m) {

		Veiculo ve = this.vDAO.findByPlaca(placa);
		boolean podeInserir = false;

		if (ve == null) {
			ve = new Veiculo();
			ve.setCor(cor);
			ve.setPlaca(placa);
			podeInserir = vDAO.save(ve) > 0 ? true : false;

		} else if (ve.getId() > 0) {

			podeInserir = eDAO.findByVeiculoWhereVagaTrue(ve.getId()) == null ? true : false;

			if (!podeInserir)
				return "config/error/veiculo/veiculoEmUso";

		}

		// insere veiculo
		if (podeInserir) {

			int idveiculo = vDAO.getMaxId();

			// insere estadia
			Estadia e = new Estadia();

			e.setIdfuncionario(1);
			e.setIdvaga(idvaga);
			e.setIdveiculo(idveiculo);

			eDAO.save(e);

			// atualiza status da vaga
			vagaDAO.updateVagaBusy(idvaga);

			m.addAttribute("veiculo", ve);

			return "config/checkInSuccess";
		}

		return "config/error/veiculo/registrarEntrada";

	}

	@PostMapping(value = "/registrarCheckOut")
	public String registrarSaida(@RequestParam String placasaida, @RequestParam Double valor, Model m) {

		// pesquisa veiculo
		Veiculo ve = this.vDAO.findByPlaca(placasaida);
		Faturamento fat;

		if (ve != null && ve.getId() > 0) {

			// pesquisa estadia
			Estadia e = eDAO.findByVeiculoWhereVagaTrue(ve.getId());

			if (e.getId() > 0) {

				// libera ou desocupa vaga
				vagaDAO.updateVagaFree(e.getIdvaga());

			}

			// atualiza horario de saida
			e.setDataHoraSaida(new Date());

			// salva estadia
			eDAO.save(e);

			// faturamento
			fat = new Faturamento();

			fat.setDataHoraPagamento(new Date());

			fat.setIdestadia(e.getId());

			fat.setIdfuncionario(1);

			fat.setObservacao("");

			fat.setValor(valor);

			// salvar faturamento
			fDAO.save(fat);
			
			m.addAttribute("valor", valor);
			m.addAttribute("veiculo", ve);
			m.addAttribute("data", fat.getDataHoraPagamento());			

			return "config/checkOutSuccess";
		}

		return "config/error";
	}

	@RequestMapping(value = "/estadia/{id}", method = RequestMethod.POST)
	public String atualizaEstadia(@PathVariable(value = "id") Integer id, @RequestParam int idfuncionario,
			@RequestParam int idvaga, @RequestParam Date dataHoraEntrada, @RequestParam Date dataHoraSaida, Model m) {

		Estadia e = eDAO.findById(id);
		e.setIdfuncionario(idfuncionario);
		e.setIdvaga(idvaga);
		e.setDataHoraEntrada(dataHoraEntrada);
		e.setDataHoraSaida(dataHoraSaida);

		eDAO.save(e);

		return "redirect:/estadia";
	}

}
