package edu.cest.control;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import edu.cest.dao.FuncionarioDAO;
import edu.cest.model.Funcionario;

@Controller
public class FuncionarioController {

	@Autowired
	private FuncionarioDAO fDAO;

	@RequestMapping("/funcionario")
	public String index(Model m) {

		List<Funcionario> l = fDAO.findAll();

		m.addAttribute("funcionarios", l);

		return "funcionario/lista";
	}

	/**
	 * 
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/funcionario/{id}", method = RequestMethod.GET)
	public String editaFuncionario(@PathVariable(value = "id") Integer id, Model m) {
		Funcionario f = fDAO.findById(id);

		// preenchendo valores do funcionario que ser�o exibidos na view html
		m.addAttribute("pageTitle", "Editar Estado");
		m.addAttribute("id", f.getId());
		m.addAttribute("nome", f.getNome());
		m.addAttribute("telefone", f.getTelefone());
		m.addAttribute("login", f.getLogin());

		return "funcionario/edita";
	}

	@RequestMapping(value = "/funcionario/{id}", method = RequestMethod.POST)
	public String atualizaFuncionario(@PathVariable(value = "id") Integer id, @RequestParam String nome,
			@RequestParam String telefone, @RequestParam Boolean isAdmin, Model m) {

		Funcionario f = fDAO.findById(id);
		f.setAdministrador(isAdmin);
		f.setNome(nome);
		f.setTelefone(telefone);

		fDAO.save(f);

		return "redirect:/funcionario";
	}

	@RequestMapping(value = "/funcionario/delete/{id}", method = RequestMethod.GET)
	public String deletaFuncionario(@PathVariable(value = "id") Integer id, Model m) {

		fDAO.delete(id);

		return "redirect:/funcionario";
	}

	/**
	 * N�o precisa de RequestMapping
	 * 
	 * @param f
	 *            - Funcionario
	 * @param m
	 *            - Model
	 * @return
	 */
	public Model getModel(Funcionario f, Model m) {
		m.addAttribute("id", f.getId());
		m.addAttribute("nome", f.getNome());
		m.addAttribute("dashboard", "/admin");
		return m;
	}

	@RequestMapping("funcionario/showForm")
	public String showForm() {
		return "funcionario/novo";
	}
}
