package edu.cest.control;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import config.FaturamentoDetalhe;
import edu.cest.dao.EstadiaDAO;
import edu.cest.dao.FaturamentoDAO;
import edu.cest.dao.FuncionarioDAO;
import edu.cest.dao.VagaDAO;
import edu.cest.dao.VeiculoDAO;
import edu.cest.model.Estadia;
import edu.cest.model.Faturamento;
import edu.cest.model.Funcionario;
import edu.cest.model.Vaga;
import edu.cest.model.Veiculo;

@Controller
public class FaturamentoController {

	@Autowired
	private FaturamentoDAO ftDAO;

	@Autowired
	private EstadiaDAO estDAO;

	@Autowired
	private FuncionarioDAO fDAO;
	
	@Autowired
	private VeiculoDAO vDAO;
	
	@Autowired
	private VagaDAO vgDAO;

	@RequestMapping("/faturamento")
	public String index(Model m) {

		List<Faturamento> l = ftDAO.findAll();
		List<FaturamentoDetalhe> lDetalhes = new ArrayList<FaturamentoDetalhe>();

		for (Faturamento f : l) {

			// Recupera estadia
			Estadia e = estDAO.findById(f.getIdestadia());

			// Recupera funcionario que realizou o faturamento
			Funcionario fun = fDAO.findById(f.getIdfuncionario());
			
			//Recupera Veiculo
			Veiculo v = vDAO.findById(e.getIdveiculo());
			
			//Recupera Vaga
			Vaga vg = vgDAO.findById(e.getIdvaga()); 

			// Ajusta Detalhes do Faturamento
			FaturamentoDetalhe fd = new FaturamentoDetalhe();
			
			fd.setIdfaturamento(f.getId());
			fd.setDataHoraPagamento(f.getDataHoraPagamento());
			fd.setObservacao(f.getObservacao());
			fd.setE(e);
			fd.setF(fun);
			fd.setVaga(vg);
			fd.setVeiculo(v);
			fd.setValor(f.getValor());
			
			lDetalhes.add(fd);
		}

		m.addAttribute("faturamentos", lDetalhes);

		return "faturamento/lista";
	}

	/**
	 * 
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/faturamento/{id}", method = RequestMethod.GET)
	public String editaFaturamento(@PathVariable(value = "id") Integer id, Model m) {
		Faturamento ft = ftDAO.findById(id);

		// preenchendo valores do faturamento que ser�o exibidos na view html
		m.addAttribute("pageTitle", "Editar Estado");
		m.addAttribute("id", ft.getId());
		m.addAttribute("idestadia", ft.getIdestadia());
		m.addAttribute("idfuncionario", ft.getIdfuncionario());
		m.addAttribute("valor", ft.getValor());
		m.addAttribute("observacao", ft.getObservacao());
		m.addAttribute("dataHoraPgamento", ft.getDataHoraPagamento());

		return "faturamento/edita";
	}

	@RequestMapping(value = "/faturamento/{id}", method = RequestMethod.POST)
	public String atualizaFuncionario(@PathVariable(value = "id") Integer id, @RequestParam int idestadia,
			@RequestParam int idfuncionario, @RequestParam Double valor, @RequestParam String observacao,
			@RequestParam Date dataHoraPagamento, Model m) {

		Faturamento ft = ftDAO.findById(id);
		ft.setId(id);
		;
		ft.setIdfuncionario(idfuncionario);
		ft.setValor(valor);

		ftDAO.save(ft);

		return "redirect:/faturamento";
	}

	@RequestMapping(value = "/faturamento/delete/{id}", method = RequestMethod.GET)
	public String deletaFaturamento(@PathVariable(value = "id") Integer id, Model m) {

		ftDAO.delete(id);

		return "redirect:/faturamento";
	}

	/**
	 * N�o precisa de RequestMapping
	 * 
	 * @param f
	 *            - Faturamento
	 * @param m
	 *            - Model
	 * @return
	 */
	public Model getModel(Faturamento ft, Model m) {
		m.addAttribute("id", ft.getId());
		m.addAttribute("nome", ft.getIdfuncionario());
		m.addAttribute("nome", ft.getValor());
		return m;
	}

}
