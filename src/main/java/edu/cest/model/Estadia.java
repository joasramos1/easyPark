package edu.cest.model;

import java.util.Date;

public class Estadia {
	
	private int id;
	
	private int idfuncionario;
	
	private int idvaga;
	
	private int idveiculo;	
	
	private Date dataHoraEntrada;
	
	private Date dataHoraSaida;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdfuncionario() {
		return idfuncionario;
	}

	public void setIdfuncionario(int idfuncionario) {
		this.idfuncionario = idfuncionario;
	}

	public int getIdvaga() {
		return idvaga;
	}

	public void setIdvaga(int idvaga) {
		this.idvaga = idvaga;
	}

	public Date getDataHoraEntrada() {
		return dataHoraEntrada;
	}

	public void setDataHoraEntrada(Date dataHoraEntrada) {
		this.dataHoraEntrada = dataHoraEntrada;
	}

	public Date getDataHoraSaida() {
		return dataHoraSaida;
	}

	public void setDataHoraSaida(Date dataHoraSaida) {
		this.dataHoraSaida = dataHoraSaida;
	}		

	public int getIdveiculo() {
		return idveiculo;
	}

	public void setIdveiculo(int idveiculo) {
		this.idveiculo = idveiculo;
	}

}
