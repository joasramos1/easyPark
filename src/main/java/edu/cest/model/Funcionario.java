package edu.cest.model;

public class Funcionario {
	
	private int id;
	private String nome;
	private String telefone;
	private String login;
	private String senha;
	private boolean isAdministrador;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}
	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	/**
	 * @return the telefone
	 */
	public String getTelefone() {
		return telefone;
	}
	/**
	 * @param telefone the telefone to set
	 */
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}
	/**
	 * @param login the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}
	/**
	 * @return the senha
	 */
	public String getSenha() {
		return senha;
	}
	/**
	 * @param senha the senha to set
	 */
	public void setSenha(String senha) {
		this.senha = senha;
	}
	/**
	 * @return the isAdministrador
	 */
	public boolean isAdministrador() {
		return isAdministrador;
	}
	/**
	 * @param isAdministrador the isAdministrador to set
	 */
	public void setAdministrador(boolean isAdministrador) {
		this.isAdministrador = isAdministrador;
	}	
	
	
}
