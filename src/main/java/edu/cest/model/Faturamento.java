package edu.cest.model;

import java.util.Date;

public class Faturamento {
	
	private int id;
	
	private int idestadia;
	
	private int idfuncionario;
	
	private Double valor;
	
	private String observacao;
	
	private Date dataHoraPagamento;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdestadia() {
		return idestadia;
	}

	public void setIdestadia(int idestadia) {
		this.idestadia = idestadia;
	}

	public int getIdfuncionario() {
		return idfuncionario;
	}

	public void setIdfuncionario(int idfuncionario) {
		this.idfuncionario = idfuncionario;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Date getDataHoraPagamento() {
		return dataHoraPagamento;
	}

	public void setDataHoraPagamento(Date dataHoraPagamento) {
		this.dataHoraPagamento = dataHoraPagamento;
	}
	
	

}
